package gearlab.com.steamopeniddemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Locale;

import gearlab.com.steamopeniddemo.repository.SteamAPI;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private String steamId;

    private Button getSteamFriendsButton;
    private TextView friendsListTextView;
    private TextView steamIdTextView;

    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.steampowered.com")
                .build();

        findViewById(R.id.auth_with_open_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSteamAuthorization();
            }
        });

        getSteamFriendsButton = (Button) findViewById(R.id.get_friends_list_button);
        getSteamFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(steamId)) {
                    loadFriendsList();
                }
            }
        });
        friendsListTextView = (TextView) findViewById(R.id.friends_list);
        steamIdTextView = (TextView) findViewById(R.id.steam_id);
    }

    private void loadFriendsList() {
        String apiKey = getString(R.string.steam_api_key);
        SteamAPI steamAPI = retrofit.create(SteamAPI.class);

        steamAPI.getFriends(apiKey, steamId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String json = response.body().string();
                    friendsListTextView.setText(json);
                } catch (IOException e) {
                    Log.e("Tag", "", e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Tag", t.toString());
            }
        });
    }

    private void showSteamAuthorization() {
        SteamAuthorizationDialog dialog = new SteamAuthorizationDialog();
        dialog.setOpenIdAuthListener(new SteamAuthorizationDialog.OpenIdAuthListener() {
            @Override
            public void onSuccess(String userId) {
                Log.e("Tag", userId);
                steamId = userId;
                getSteamFriendsButton.setEnabled(true);
                steamIdTextView.setText(String.format(Locale.getDefault(), "Your Steam ID is: %s", steamId));
            }

            @Override
            public void onError(CharSequence description) {
                Log.e("Tag", description.toString());
            }
        });
        dialog.show(getFragmentManager(), "TAG");
    }
}
