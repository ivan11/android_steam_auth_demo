package gearlab.com.steamopeniddemo.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ivanphytsyk on 2/24/17.
 */

public interface SteamAPI {
    @GET("/ISteamUser/GetFriendList/v0001/?relationship=friend")
    Call<ResponseBody> getFriends(@Query("key") String apiKey, @Query("steamid") String userId);
}
