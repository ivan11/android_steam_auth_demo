package gearlab.com.steamopeniddemo;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ivanphytsyk on 2/23/17.
 */

public class SteamAuthorizationDialog extends DialogFragment {

    private static final String OPEN_ID_AUTH_URL = "https://steamcommunity.com/login/home";
    private static final Pattern SUCCESS_LOGIN_REGEX = Pattern.compile("http://steamcommunity.com/profiles/.*/.*");

    private OpenIdAuthListener openIdAuthListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            dismiss();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_steam_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final WebView webView = (WebView) view.findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Matcher matcher = SUCCESS_LOGIN_REGEX.matcher(url);
                if (matcher.find()) {
                    webView.stopLoading();
                    String userId = url.substring(url.lastIndexOf("profiles/") + "profiles/".length(), url.lastIndexOf("/home"));
                    if (openIdAuthListener != null) {
                        openIdAuthListener.onSuccess(userId);
                        dismiss();
                    }
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                if (openIdAuthListener != null) {
                    openIdAuthListener.onError(description);
                    dismiss();
                }
            }
        });
        webView.loadUrl(OPEN_ID_AUTH_URL);
    }

    public void setOpenIdAuthListener(OpenIdAuthListener openIdAuthListener) {
        this.openIdAuthListener = openIdAuthListener;
    }

    public interface OpenIdAuthListener {
        void onSuccess(String userId);

        void onError(CharSequence description);
    }
}
